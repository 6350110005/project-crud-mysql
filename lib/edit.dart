import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'product.dart';

class Edit extends StatefulWidget {
  final Product product;

  Edit({required this.product});

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
  // This is for text to edit
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  // Http post request
  Future editStudent() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.product.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descriptionController.text
      },
    );
  }

  void _onConfirm(context) async {
    await editStudent();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.product.name);
    priceController = TextEditingController(text: widget.product.price.toString());
    descriptionController = TextEditingController(text: widget.product.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit product"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.pink,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: "Drugs:",
                    hintText: "Enter drugs name",
                  ),
                )),
            Container(
                child: TextField(
                  controller: priceController,
                  decoration: InputDecoration(
                    labelText: "Price:",
                    hintText: "Enter price",
                  ),
                )),
            Container(
                child: TextField(
                  controller: descriptionController,
                  decoration: InputDecoration(
                    labelText: "Description:",
                    hintText: "Enter description",
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
