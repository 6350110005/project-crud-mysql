import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'product.dart';
import 'details.dart';
import 'create.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Product>> products;

  @override
  void initState() {
    super.initState();
    products = getProductList();
  }

  Future<List<Product>> getProductList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load drugs');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Drugs List'),
      ),

      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            UserAccountsDrawerHeader(
              accountName: Text(
                "Medicine",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ),
              ),
              accountEmail: Text(
                "mrMedicine@gmail.com",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/images/172835.png'),
                radius: 60,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.pink,
              ),
              title: const Text('Home',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.pink,),),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            AboutListTile( // <-- SEE HERE
              icon: Icon(
                Icons.account_circle,
                color: Colors.pink,
              ),
              child: Text('About',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.pink,),),
              applicationIcon: Icon(
                Icons.account_circle,
                color: Colors.pink,
              ),
              applicationName: 'About',
              aboutBoxChildren: [
                Image.asset("assets/images/IMG_20220718_160420.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("คนที่ 1",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
                Text("รหัส : 6350110005",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
                Text("ชื่อ : ณัฐนนท์ ส่งศรีจันทร์",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
                SizedBox(height: 20,),
                Image.asset("assets/images/293039182_2501543417359842_2713414559648052017_n.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("คนที่ 2",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
                Text("รหัส : 6350110014",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
                Text("ชื่อ : พงศ์อรรถ แก่นอิน",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  color: Colors.pink,
                ),),
              ],
            ),
          ],
        ),
      ),

      body: Center(
        child: FutureBuilder<List<Product>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Card(
                  child: ListTile(
                    leading: Icon( Icons.medical_services,color: Colors.pink, size: 40,),
                    trailing: Icon(Icons.view_list),
                    title: Text(
                      data.name,
                      style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.pink,),
                    ),
                    subtitle: Text(
                      data.price,
                      style: TextStyle(fontSize: 20),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Details(product: data)),
                      );
                    },
                  ),
                );
              },
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create()),
          );
        },
      ),
    );
  }
}
