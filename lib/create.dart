import 'package:crudmysql/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class Create extends StatefulWidget {
  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<Create> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();


  // Http post request to create new data
  Future _createProduct() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "description": descriptionController.text,
        "price": priceController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createProduct();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  final _FormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create"),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Form(
          key: _FormKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  child: TextFormField(
                    validator: (input) {

                      if(input!.isEmpty) {
                        return "Please enter the name of the drug.";
                      }
                      return null;
                    },
                    controller: nameController,
                    decoration: InputDecoration(
                      labelText: "Drugs:",
                      hintText: "Enter drugs name",
                    ),
                  )),
              Container(
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    validator: (input) {

                      if(input!.isEmpty) {
                        return "Please enter price.";
                      }
                      return null;
                    },
                    controller: priceController,
                    decoration: InputDecoration(
                      labelText: "Price:",
                      hintText: "Enter Price",
                    ),
                  )),
              Container(
                  child: TextFormField(
                    validator: (input) {

                      if(input!.isEmpty) {
                        return "Please enter a description.";
                      }
                      return null;
                    },
                    controller: descriptionController,
                    decoration: InputDecoration(
                      labelText: "Description:",
                      hintText: "Enter Description",
                    ),
                  )),
              SizedBox(height: 15,),


              //   SizedBox(height: 15,),
              //  Text('Name:'),
              //  TextFormField(
              //    validator: (input) {

              //      if(input!.isEmpty) {
              //        return "กรุณากรอกชื่อด้วย";
              //      }
              //      return null;
              //    },
              //  ),
              //  SizedBox(height: 15,),
              //  Text('Price:'),
              //  TextFormField(
              //    keyboardType: TextInputType.number,
              //    inputFormatters: [
              //      FilteringTextInputFormatter.digitsOnly,
              //  ],
              //  validator: (input) {

              //    if(input!.isEmpty) {
              //      return "กรุณากรอกราคาด้วย";
              //    }
              //    return null;
              //  },
              //),
              //SizedBox(height: 15,),
              //Text('Description:'),
              //TextFormField(
              //  validator: (input) {

              //    if(input!.isEmpty) {
              //      return "กรุณากรอกชื่อยาด้วย";
              //    }
              //    return null;
              //  },
              //),
              Row(
                children: [
                  Expanded(child: RaisedButton(
                    onPressed: () {
                      bool pass = _FormKey.currentState!.validate();

                      if(pass){
                        // ส่งไปที่ Sever
                        _onConfirm(context);

                      }
                    },
                    child: Text("Save",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold)),
                  ),)
                ],
              ),
              Row(
                children: [
                  Expanded(child: OutlinedButton(
                    onPressed: (){
                      _FormKey.currentState!.reset();
                    },
                    child: Text("Clean up",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  ),),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}